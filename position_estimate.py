from telegram import ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import sys, os
import math


def start(bot, update):

    call_me_hand = u'\U0001F919'

    greet_ru = "<b>На связи Pentagon!</b>" + call_me_hand + "\n\nПриветствуем в нашем калькуляторе для расчёта позиции на бирже <b>Bitmex</b>!\n\n" +\
               "Введите /help для дальнейших инструкций.\n\n<b>Всегда соблюдайте риск-менеджмент!</b>"

    greet_eng = 'Hi there! I am <b>Pentagon Estimate Bitmex Position Bot</b>!\n\nPlease enter /help to get further instructions!'
    
    update.effective_message.reply_text(greet_ru, parse_mode=ParseMode.HTML, timeout=15)
   
def help_me(bot, update, chat_data):
    msg_part1_en = "This bot counts Profit/Risk ratio and recommended position size (in XBTUSD contracts).\n"
    msg_part2_en = "To function bot require following data:\n\n"
    msg_part3_en = "Your deposit int BTC\nCurrent BTC rate\nDeal type(\"-1\" for short and \"1\" for long)\nRisk (0.5% - low;\n         1-2% - recommended;\n"
    msg_part4_en = "         3-4% - high;)\nEntry price\nStop loss price\nTake profit price\n\n"
    msg_part5_en = "Please!!! be sure that EACH value in a SEPARATE line but all data is in a SINGLE message! It is important to get proper result!!\n\n" +\
                "To calculate result based on your data, copy the message below, fill it with your data and send it to our bot!"

    msg_part6_en = "/count_pos\nYour deposit int BTC: 0.5\nCurrent BTC rate: 3500\nDeal type: -1\nRisk: 0.5%\nEntry price: 3510\nStop loss price: 3550\nTake profit price: 3400"



    tennis_ball = u'\U0001F3BE'
    full_moon = u'\U0001F315'
    large_red = u'\U0001F534'
    white_circle = u'\U000026AA'
    cross = u'\U0000274C'
    check = u'\U00002705'


    msg_part1_ru = "Этот бот расчитывает соотношение <b>Риск/Прибыль</b> и рекомендует Вам размер позиции (в контрактах <b>XBTUSD</b>).\n\n"
    msg_part2_ru = "<b>Необходимые параметры для расчёта позиции:</b>\n\n"
    msg_part3_ru = "1. Размер Вашего депозита (в <b>XBT</b>)\n" +\
                   "2. Актуальный курс <b>XBT</b> для биржи <b>Bitmex</b>\n" +\
                   "3. Тип сделки (\"-1\" для \"Short\", \"1\" для \"Long\")\n" +\
                   "4. Риск (0.5% - " + tennis_ball + "низкий;\n" +\
                   "              1-2% - " + full_moon   + "рекомендованный;\n" +\
                   "              3-4% - " + large_red   + "высокий;)\n" + \
                   "5." + white_circle + "Точка входа в сделку\n" + \
                   "6." + cross + "Стоп-лосс для сделки\n" + \
                   "7." + check + "Тейк-профит для сделки\n\n"
    
    msg_part4_ru = "Убедитесь что параметры введены именно в необходимой последовательности для правильного расчёта рисков!\n" +\
                   "Все параметры должны быть отправленны <b>ОДНИМ</b> сообщением!\n" +\
                   "Каждый параметр должен быть написан <b>C НОВОЙ СТРОКИ</b>!\n\n"

    msg_part5_ru = "Для расчёта результа на основании Ваших данных, скопируйте приведенное ниже сообщение, впишите свои данные, и отправьте нашему боту!"

    msg_part6_ru = "/count_pos\nРазмер депозита в XBT: 0.5\nТекущий курс XBT: 3500\nТип сделки: -1\nРиск: 0.5%\nТочка входа: 3510\nСтоп-лосс: 3550\nТейк-профит: 3400"    





    update.effective_message.reply_text(msg_part1_ru + msg_part2_ru + msg_part3_ru + msg_part4_ru + msg_part5_ru, parse_mode=ParseMode.HTML, timeout=15)
    update.effective_message.reply_text(msg_part6_ru, parse_mode=ParseMode.HTML, timeout=15)

def roundup(x):
    return int(math.ceil(x / 100.0)) * 100

def count_position(bot, update, chat_data):
    try:
        msg = update.effective_message.text
        splited = msg.split("Размер депозита")[1].split("\n")

        if len(splited) != 7:
            update.effective_message.reply_text("Lack of data! Please check that you have all fields filled, and each value is in a separate lane! It is good idea to compare your input with an example form /help")
            return

        try:
            btc_balance = float(splited[0].split(":")[1].strip())
            if btc_balance < 0:
                raise Exception("Balance less than 0")
        except:
            update.effective_message.reply_text("An error occured while reading \"BTC balance\" (field 1). Please check that you have proper data filled! It is good idea to compare your input with an example form /help")
            return
        
        try:
            btc_rate = float(splited[1].split(":")[1].strip())
            if btc_rate < 0:
                raise Exception("Rate less than 0")
        except:
            update.effective_message.reply_text("An error occured while reading \"BTC rate\" (field 2). Please check that you have proper data filled! It is good idea to compare your input with an example form /help")
            return

        try:
            is_long = int(splited[2].split(":")[1].strip())
            if is_long != 1 and is_long != -1:
                raise Exception('Not 1 or -1!')
        except:
            update.effective_message.reply_text("An error occured while reading \"Deal type\" (field 3). Please check that you have proper data filled! It should be \"1\" or \"-1\"!  It is good idea to compare your input with an example form /help")
            return

        try:
            risk_rate = float(splited[3].split(":")[1].split("%")[0].strip())
            if risk_rate > 100 or risk_rate < 0:
                raise Exception("Bad data risk rate")
        except:
            update.effective_message.reply_text("An error occured while reading \"Risk rate\" (field 4). Please check that you have proper data filled! It is good idea to compare your input with an example form /help")
            return

        try:
            entry_price = float(splited[4].split(":")[1].strip())
            if entry_price < 0:
                raise Exception("Entry less than 0")
        except:
            update.effective_message.reply_text("An error occured while reading \"Entry price\" (field 5). Please check that you have proper data filled! It is good idea to compare your input with an example form /help")
            return

        try:
            stop_loss = float(splited[5].split(":")[1].strip())
       
            if stop_loss < 0:
                raise Exception("Stop loss less than 0")
        except:
            update.effective_message.reply_text("An error occured while reading \"Stop loss\" price (field 6). Please check that you have proper data filled! It is good idea to compare your input with an example form /help")
            return

        try:
            take_profit = float(splited[6].split(":")[1].strip())
            
            if take_profit < 0:
                raise Exception("Rate less than 0")
        except:
            update.effective_message.reply_text("An error occured while reading \"Take profit\" price (field 7). Please check that you have proper data filled! It is good idea to compare your input with an example form /help")
            return
        

        returned_1 = ""
        returned_2 = ""

        if is_long == 1:
            returned_1 = (take_profit - entry_price) / (entry_price - stop_loss)
        else:
            returned_1 = (entry_price - take_profit) / (stop_loss - entry_price)
        returned_2 = -int(risk_rate / 100.0 * btc_balance * btc_rate / (stop_loss / entry_price - 1))

        update.effective_message.reply_text("Cоотношение Риск/Прибыль:         " + str(round(returned_1, 3)) + "\nРекомендуемый размер позиции: " + str(roundup(returned_2)))

    except Exception as e:
        update.effective_message.reply_text('Something has gone wrong! ' + str(e))

def stop(bot, update, chat_data):
    update.effective_message.reply_text('Bot stopped. Bye!')


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Run bot."""
    updater = Updater("640314060:AAEWEv1Al03kQyJ6sqrTIiElrLFryJ1ig1k")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("count_pos", count_position,
                                  pass_chat_data=True))
    dp.add_handler(CommandHandler("help", help_me,
                                  pass_chat_data=True))
    dp.add_handler(CommandHandler("stop", stop, pass_chat_data=True))


    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Block until you press Ctrl-C or the process receives SIGINT, SIGTERM or
    # SIGABRT. This should be used most of the time, since start_polling() is
    # non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()